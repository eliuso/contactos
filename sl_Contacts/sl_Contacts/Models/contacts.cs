﻿using System.ComponentModel.DataAnnotations;

namespace sl_Contacts.Models
{
    public class contacts
    {
        [Key]
        public int ContactId { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Ingrese el Campo {0}")]
        [StringLength(30, ErrorMessage = "El Tamaño del Campo {0} debe estar entre {2} y {1} Caracteres", MinimumLength = 3)]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "Ingrese el Campo {0}")]
        [StringLength(30, ErrorMessage = "El Tamaño del Campo {0} debe estar entre {2} y {1} Caracteres", MinimumLength = 3)]
        public string LastName { get; set; }

        [Display(Name = "Direccion")]
        [StringLength(60, ErrorMessage = "El Tamaño del Campo {0} debe estar entre {2} y {1} Caracteres", MinimumLength = 3)]
        public string Address { get; set; }

        [Display(Name = "Número Telefono")]
        [Required(ErrorMessage = "Ingrese el Campo {0}")]
        public string PhoneNumber { get; set; }
    }
}
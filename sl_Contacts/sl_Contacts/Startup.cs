﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sl_Contacts.Startup))]
namespace sl_Contacts
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

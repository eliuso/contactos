﻿using System.Data.Entity;

namespace sl_Contacts.Models
{
    public class ContactsContext : DbContext
    {
        public ContactsContext() : base("name=ContactContext")
        {
        }

        public DbSet<contacts> contacts { get; set; }
    }
}

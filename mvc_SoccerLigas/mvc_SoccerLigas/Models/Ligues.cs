﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace mvc_SoccerLigas.Models
{
    [Serializable]
    [XmlRoot("Category"), XmlType("Name")]
    public class Ligues
    {
        public string category { get; set; }
        public string name { get; set; }
        public string gid { get; set; }
        public string id { get; set; }
        public string file_group { get; set; }
        public string iscup { get; set; }
        public string matches { get; set; }
        public string date { get; set; }
        public string formatted_date { get; set; }
        public string time { get; set; }
        public string LTeam { get; set; }
        public string VTeam { get; set; }
        public string ftScore { get; set; }
    }
}
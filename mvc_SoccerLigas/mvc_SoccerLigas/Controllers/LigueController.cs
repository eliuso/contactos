﻿using mvc_SoccerLigas.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace mvc_SoccerLigas.Controllers
{
    public class LigueController : Controller
    {
        // GET: Ligue
        public ActionResult Index()
        {
            DataSet ds = new DataSet();
            List<SelectListItem> dropDownList = new List<SelectListItem>();

            string xmlData = Server.MapPath("~/App_Data/soccer.xml");
            ds.ReadXml(xmlData);

            var SLigue = new List<Ligues>();

            SLigue = (from rows in ds.Tables["category"].AsEnumerable()
                      select new Ligues
                      {
                          name = rows["name"].ToString(),
                          id = rows["category_id"].ToString()
                      }).ToList();
            foreach (var item in SLigue)
            {
                dropDownList.Add(new SelectListItem() { Text = item.name, Value = item.id });
            }

            ViewBag.DropDownValues = dropDownList;
            return View();
        }
        [HttpGet]
        public JsonResult GetDetailsLiga(int? pId)
        {

            List<Ligues> SLigue = new List<Ligues>();
            try
            {
                DataSet ds = new DataSet();
                string xmlData = Server.MapPath("~/App_Data/soccer.xml");
                ds.ReadXml(xmlData);


                SLigue = (from m in ds.Tables["match"].AsEnumerable()
                          join lt in ds.Tables["localteam"].AsEnumerable() on m.Field<int>("match_id") equals lt.Field<int>("match_id")
                          join lv in ds.Tables["visitorteam"].AsEnumerable() on m.Field<int>("match_id") equals lv.Field<int>("match_id")
                          //join ht in ds.Tables["ht"].AsEnumerable() on m.Field<int>("match_id") equals ht.Field<int>("match_id")
                          join ft in ds.Tables["ft"].AsEnumerable() on m.Field<int>("match_id") equals ft.Field<int>("match_id")
                          //join et in ds.Tables["et"].AsEnumerable() on m.Field<int>("match_id") equals et.Field<int>("match_id")
                          where m.Field<int>("matches_id").Equals(pId)
                          select new Ligues
                          {
                              time = m["time"].ToString(),
                              formatted_date = m["formatted_date"].ToString(),
                              LTeam = lt["name"].ToString(),
                              VTeam = lv["name"].ToString(),
                              ftScore= ft["score"].ToString()

                          }).ToList();

            }
            catch (System.Exception ex)
            {
                ViewBag.msgError = ex.InnerException.ToString() + "-" + ex.Message.ToString();
                throw;
            }
            return Json(new { SLigue }, JsonRequestBehavior.AllowGet);
        }
    }
}